Quick start
===========

This is a boilerplate for the belgian quaterly Médor, based on HTML2print.

Licence
-------

This file is part of HTML2print.

HTML2print is free software: you can redistribute it and/or modify it under the
terms of the GNU Affero General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

HTML2print is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
details.

You should have received a copy of the GNU Affero General Public License along
with HTML2print.  If not, see <http://www.gnu.org/licenses/>.


Pre-requisites
--------------

Clone and enter the repository

    git clone git@gitlab.constantvzw.org:medor/boilerplate.git
    cd boilerplate

Make sure you have all the system-wide dependencies

    sudo apt-get update
    sudo apt-get install python-dev libxml2-dev libxslt1-dev
    sudo apt-get install libffi-dev
    sudo apt-get install python-virtualenv
    sudo apt-get install npm
    sudo apt-get install nodejs-legacy
    sudo npm -global install bower


Install the js and css dependencies (it will create a "vendors" directory)

    bower install

Install the python dependencies

    virtualenv --no-site-packages venv
    source venv/bin/activate
    pip install -r requirements.txt

Copy the exemple settings file and set the credentials for medor.coop. They will
be used to fetch the articles from the plateform

    cp bin/settings.example.py bin/settings.py
    nano bin/settings.py

Pull the stories in the stories directory

    make stories

Generate the table of contents for HTML2print to work

    make json


Using HTML2print
----------------

Starts the serveur

    python2 -m SimpleHTTPServer

Alternatively, There is a little script based on SimpleHTTPServer that forces
the browser not to cache the files

    python2 bin/serveit.py

Visit the URL:

    http://localhost:8000

keeping everything up-to-date
=============================

Updating the stories
--------------------

Enter the virtualenv if it is not yet the case

    source venv/bin/activate

Pull the stories in the stories directory

    make stories

Updating the table of contents
------------------------------

Enter the virtualenv if it is not yet the case

    source venv/bin/activate

Generate the table of contents for HTML2print to work

    make json
