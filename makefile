# This file is part of HTML2print.
# 
# HTML2print is free software: you can redistribute it and/or modify it under the
# terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
# 
# HTML2print is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
# details.
# 
# You should have received a copy of the GNU Affero General Public License along
# with HTML2print.  If not, see <http://www.gnu.org/licenses/>.


SHELL := /usr/bin/env bash


STORIES_FILES = $(shell find build/stories/ -type f -name '*.html')
CSS_FILES = $(patsubst %.less, %.css, $(STORIES_FILES))

LESS_FILES = $(shell find layout -type f -name '*.less')
CSS_FILES = $(patsubst %.less, %.css, $(LESS_FILES))

LAYOUT_FILES = $(shell find layout -type f -name '*.html')


all: json stories
css: build/css/main.css $(CSS_FILES)
json: build/js/src.json


layout/%.css : layout/%.less
	lessc $< > $@


build/js/src.json : $(LAYOUT_FILES)
	mkdir -p $(@D)
	python2 bin/makejson.py --outfile $@ $^


.PHONY: stories
stories:
	mkdir -p build/$(@)
	python2 bin/pullstories.py build/$(@)


.PHONY: clean
clean:
	rm -fr build
