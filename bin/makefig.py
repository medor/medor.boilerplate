#! /usr/bin/env python3


# Copyright (C) 2015-2017 Alexandre Leray (Open Source Publishing)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# Fetches the images of an HTML document and fixes links to them
#
# Usage:
#
#     ./generate.py infile.md outfile.html


import html5lib
import logging
from html5lib.filters import _base


logger = logging.getLogger(__name__)
formatter = logging.Formatter('[%(levelname)s] %(message)s')
handler = logging.StreamHandler()
handler.setFormatter(formatter)
logger.addHandler(handler)
logger.setLevel(logging.INFO)


class ImgSrcFilter(_base.Filter):
    """
    """
    def __iter__(self):
        i = 1

        for token in _base.Filter.__iter__(self):
            if token["type"] == "EmptyTag" and token['name'] == "img":
                src = token["data"][(None, 'src')]

                print("/* {} */".format(src))
                print('flow-main figure:nth-of-type({0}) {{ .flow-into(~"flow-fig{0}"); }}'.format(i))
                print('.flow-fig{0} {{ .flow-from(~"flow-fig{0}"); }}'.format(i))
                print('\n')

                i += 1

            yield token


def pull_images(infile):
    print("processing {}".format(infile.name))
    dom = html5lib.parseFragment(infile.read(), treebuilder="etree")
    walker = html5lib.getTreeWalker("etree")

    stream = walker(dom)
    stream = ImgSrcFilter(stream)

    s = html5lib.serializer.HTMLSerializer(quote_attr_values=True,
                                            omit_optional_tags=False)

    output = s.render(stream)
    return output


if __name__ == '__main__':
    import argparse
    import sys

    parser = argparse.ArgumentParser()
    parser.add_argument('infile', type=argparse.FileType('r'), default=sys.stdin)
    parser.add_argument('-o', '--outfile', nargs='?', type=argparse.FileType('w'), default=sys.stdout)
    args = parser.parse_args()

    out = pull_images(args.infile)
    #  args.outfile.write(out.encode("utf-8"))
